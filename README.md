## Under development

# authnetsoap

Lightweight replacement for Visa's Authorize.net SDK.

### Motivation

The SDK Python package provided by Authorize.net depends on pyxb 1.2.5 which causes issues on my Amazon server.

I also did not like the idea that JSON in their "REST API" [needs to be ordered](https://developer.authorize.net/api/reference/index.html), so with no SDK for my
setup, I began to use plain ol SOAP.

Benfeits to authnetsoap over official SDK:
* low requirements  (verify): No pyxb needed: lxml is lighter (verify) secure against attacks (billion laughs, etc TODO: Verify this)
* don't need to learn the SDK terminology as well as the API terminology: look up in the API docs, and provide the arguments you need. (TODO: Is an example warranted here?)
* configuration in os variables
* some minimal Django support, for configuration (checks Django settings module)

### Dependencies

* lxml - To run the tests for official Python SDK you would need lxml anyway.
* requests

### Usage

Set environment these variables, which are picked up by the `config` module.

  * AUTHORIZE_NET_API_URL
  * AUTHORIZE_NET_HOSTED_PAYMENT_URL
  * AUTHORIZE_NET_KEY
  * AUTHORIZE_NET_NAME

Functions that are like `processWhateverRequest` will handle the request and
response logic for you and return appropriate data: i.e. `processAuthenticateTestRequest()`

Functions like `whateverRequest` will return a string of xml for that request,
populated with the arguments you passed to it. You then handle sending the request,
and the response.

Functions for processing response strings are like `processWhateverResponse`,
and return either response info, error info, or
 simply true/false as appropriate.

#### Notation / Style

When the argument refers directly to something in Authorize.NET's API, such
as a customer profile id, we use camelCase, the same notation as Authorize.NET.
This is in contrast to PEP [TODO what pep is that] but helps ease of use,
especially when cross referencing the API.

The idea is that we want to pass in dicts of info for requests and recieve
dicts of info for responses. With this in mind, camelCase is used for both
function arguments and return values. i.e.,
```
getWhateverRequest(**{fooBar: "value"})
# or
getWhateverRequest(fooBar="value")

handleWhateverRequest("<fooBar>value</fooBar>"):
  returns { "fooBar": "value" }

```
### Implementaions

Compared to the official SDK, this is a very minimal project, lacking
support for every use case. Not all features have been implemented, and most
areas have only a subset of the requests/responses.

Three areas have been partially implemented: Customer Profiles,
Payment Transactions, Transaction Reporting. The functionality
to "Test Your Authentication Credentials" is also implemented.

* authenticatetest.py

  Uses `config` settings to execute an `authenticateTestRequest` [authenticateTestRequest](https://developer.authorize.net/api/reference/index.html#myTab)

* customer.py

  Customer Profiles

* payments.py

  Payment Transactions

* reporting.py

  Transaction Reporting

* request.py

  Useful and common functions

### Tests

Basic unittests *some of which connect to a sandbox account*.

TODO: Make the sandbox creds configurable for testing! Or separate them out into a functional test, leaving basic checks to unit testing

## Use cases

(TODO move this up above impl? goes along with the lists of features implemented)

The intended use cases to be covered are:
* customer pays one-time using a hosted payment page

* customer has a log in or ID and wishes to save payment information

* customer wants to pay with an existing payment method

* customer wants to add another payment method

* customer wants to remove a payment method

* the merchant wants to collect unsettled transactions (to mark as pending settlement)

* the merchant wants to collect settled transactions (to mark as settled)

TODO: Declined? Errors?

#### testing

Unit

In root directory (outside of authnetsoap/) run python -m unittest discover

Functional

TODO: tests.py online sandbox test is best used as its own script and not part of a unittest suite like it is now


#### notes to myself....

IMPLEMENT / DEFER
Here's the full functionality list and what needs to be implemented (Y) / skipped (N)
Customer Profiles
-----------------
Create Customer Profile - Y
Get Customer Profile - Y
Get Customer Profile IDs - N
Update Customer Profile - N, we don't send customer email to auth net, and we dont anticipate changing our merchant assigned IDs
Delete Customer Profile - Y
Create Customer Payment Profile - Y, add new to profile
Get Customer Payment Profile
Get Customer Payment Profile List
Validate Customer Payment Profile
Update Customer Payment Profile - N, seems dangerous, would rather customer add new and delete old
Delete Customer Payment Profile - Y
Create Customer Shipping Address - N We have better features with our own stored locations
Get Customer Shipping Address - N
Update Customer Shipping Address - N
Delete Customer Shipping Address - N
Get Accept Customer Profile Page - TBD,
   we create customer stuff on our end and
   would just need a form to send the auth
   net payment info - but to avoid PCI, we want this,
   unless we can have the customer already logged in
Create a Customer Profile from a Transaction - TBD, Probably useful


Payment Transactions
--------------------
Charge a Credit Card - Y*
Authorize a Credit Card - N
Capture a Previously Authorized Amount - N
Capture Funds Authorized Through Another Channel - N
Refund a Transaction - Y
Void a Transaction - N
Update Split Tender Group -N
Debit a Bank Account Y*
Credit a Bank Account Y*
Charge a Customer Profile Y!
Charge a Tokenized Credit Card N
Create an Accept Payment Transaction N
Get an Accept Payment Page - Y
\*Not sure if we really will use this


Transaction Reporting
---------------------

Get Settled Batch List Y
Get Transaction List Y
Get Unsettled Transaction List Y
Get Customer Profile Transaction List N
Get Transaction Details N*
Get Batch Statistics N
Get Merchant Details N
Get Account Updater Job Summary N
Get Account Updater Job Details N
\* not sure
