import datetime
from pdb import set_trace
set_trace()
import requests
from unittest import mock
from unittest.mock import patch
from authnetsoap import common, customer, authenticatetest

"""
Uses sandbox credentials to 

TODO: Allow OS environ override for devs to use their own sandbox to run the test
"""
API_URL = 'https://apitest.authorize.net/xml/v1/request.api'
KEY = '8qc2A2q65h5YJB94'
NAME = '3C47tKn3T'

if __name__ == '__main__':
    print("Loading mocks for sandbox config ")
    patcher = patch('authnetsoap.common.config',
                    AUTHORIZE_NET_KEY=KEY,
                    AUTHORIZE_NET_NAME=NAME,
                    AUTHORIZE_NET_API_URL=API_URL)
    patcher.start()

    # Show the API key by making unused element with auth
    foo = common.make_root("foo")
    auth = common.addMerchantAuthentication(foo)
    # if no input, bail, in case we dont want to connect
    print("This will connect using"
          "{0}".format(auth.findall("name")[0].text))
    print("To {0}".format(API_URL))
    x = input("Enter any character to continue.")
    if x is not None and x != " ":
        # continuing with test ...
        print("Ok...")
        # just ask sandbox if these creds are ok
        print("Checking auth creds")
        success = authenticatetest.processAuthenticateTestRequest()
        if not success:
            raise RuntimeError("FAILED: sandbox credentials incorrect")
        else:
            print("Credentials ok.")

        # Make a customer profile
        print("Making customer profile ")
        uid = "{0}@here.com".format(datetime.datetime.now())
        inputs = {
            "email": uid,
            "creditCardNumber": "4111111111111111",
            "expirationDate": "2020-12"
        }
        expected = {
            "success": True,
        }
        check = customer.processCreateCustomerProfile(inputs)
        for e in expected:
            if check[e] != expected[e]:
                raise RuntimeError("ERROR {0} {1}".format(check[e], expected[e]))
        print(check)

        customer_profile_id = check.get('customerProfileId')
        print("The customer was assigned customer_profile_id {0}".format(customer_profile_id))

        # The customer adds a new payment method, BANK ACCOUNT

        # We run a transaction against BANK ACCOUNT payment method

        # We check the results of the transaction

        # We run a transaction against CC payment method

        # We check the results of the transaction

        # We delete a payment method

        # We delete the customer profile, no longer needed
        print("Deleting customer profile {0}".format(customer_profile_id))
        result = customer.processDeleteCustomerProfile({
            "customerProfileId": customer_profile_id
        })

        print(result)
    print("Stopping mock")
    patcher.stop()
