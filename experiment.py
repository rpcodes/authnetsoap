"""
Parse xml and generate code that will take a python dict of values
and insert those values into an xml request

i.e. calling
request_name({
"elementWithValue": { "text": "value" },
"elementWithAttrs" : { "text" : "", "attrs": { "foo": "bar" }}
"elementWithListOfChildren" : {
  "children" : [
    { "elementWithValue" : {"text": "value" } }
  ]
}"
})

will generate
<requestName>
    <elementWithValue>value</elementWithValue>
    <elementWithAttrs foo="bar"></elementWithAttrs>
    <elementWithListOfChildren>
      <elementWithValue>value</elementWithValue>
    </elementWithListOfChildren>
</requestName>

the function request_name is generated from any XML of the form shown, in the example


"""
