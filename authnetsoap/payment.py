"""
Payment Transactions
https://developer.authorize.net/api/reference/index.html#payment-transactions
--------------------
Charge a Credit Card
Refund a Transaction
Debit a Bank Account *
Credit a Bank Account *
Charge a Customer Profile
Get an Accept Payment Page - this may become its own file
\*Not sure if we really will use this
"""
