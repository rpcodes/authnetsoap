from lxml import etree as ET
from . import config
# This is the requests library, NOT out request module
import requests
"""
Base functionality.
"""
namespace = 'AnetApi/xml/v1/schema/AnetApiSchema.xsd'
NSMAP = {None: namespace, }
format_for_datetime = '%Y-%m-%dT%H:%M:%SZ'


def get_unqualified_tag_name(qualified_elem_tag):
    return ET.QName(qualified_elem_tag).localname

def process(requestCallback, responseCallback, input):
    req = requestCallback(**input)
    resp = send(req)
    resp = resp.content.decode('utf-8')
    return responseCallback(resp)

def checkMessage(root):
    """
    Input
    root - an lxml.etree.Element instance
    Output
    Returns the success/errors code and text as a tuple (code, text, success)

    The third return param returns true if Ok false if not (indicating an Error)
    May raise ValueError or AttributeError if trying to parse wrong format

    TODO: hould this be in a response.py since its for checking responses?
    """
    messages = root.getchildren()[0]
    success = False
    if messages.tag == "{{{0}}}messages".format(namespace):
        resultCode = messages.getchildren()[0]
        if resultCode.tag == "{{{0}}}resultCode".format(namespace):
            success = bool(''.join(resultCode.text.split()).upper())
        else:
            raise ValueError("The messages element has no resultCode element.")
        message = messages.getchildren()[1]
        code = message.getchildren()[0].text
        text = message.getchildren()[1].text
        return (code, text, success)
    else:
        raise ValueError("The messages element has no message element.")
    raise ValueError("This response does not have messages element")

def findall(root, tag_name):
    """
    Uses Element.findall, and the Anet API Schema namespace, to return
    a list of all matching elements with tag_name

    root -  an lxml.etree.Element
    """
    return root.findall("{{{0}}}{1}".format(namespace, tag_name))

def make_child(root, tag_name, text):
    """
    Convenience for creating root tag with Anet API Schema namespace.
    root -  an lxml.etree.Element
    tag_name - child to add to the root
    text - a text value to put into the element, i.e. Element.text = text
    """
    elem = ET.SubElement(root, tag_name, nsmap=NSMAP)
    if text:
        elem.text = text
    return elem

# Build a request using lxml given the desired parameters
def make_root(tag_name):
    """Convenience for creating root tag with Anet API Schema namespace."""
    return ET.Element(tag_name, nsmap=NSMAP)

def addMerchantAuthentication(root):
    """Add merchant credentials to the XML root"""
    ma = ET.SubElement(root, "merchantAuthentication")
    ET.SubElement(ma, "name").text = config.AUTHORIZE_NET_NAME
    ET.SubElement(ma, "transactionKey").text = config.AUTHORIZE_NET_KEY
    return ma

def send(xml_request):
    """
    Sends request to config.AUTHORIZE_NET_API_URL.

    Accepts bytes, string, or etree.Element instance.

    Decode the xml bytes to a string if zml_request isinstance bytes

    Send the request with python requests library to the server in
    config / settings
    """
    if isinstance(xml_request, ET._Element):
        xml_request = ET.tostring(xml_request)
    if isinstance(xml_request, bytes):
        xml_request = xml_request.decode('utf-8')
    if not isinstance(xml_request, str):
        raise ValueError("Expecting utf-8 bytes or string")
    headers = {'Content-Type': 'application/xml'}
    return requests.post(config.AUTHORIZE_NET_API_URL,
      data=xml_request,
      headers=headers)
