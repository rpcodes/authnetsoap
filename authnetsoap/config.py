"""
Configuration.
Authorize.net requests must be authenticated using the provided name and key.
Also note that to run in production, you also must set the URLs. Sandbox
URLs have been provided, and tests use sandbox credentials as well to ensure
the production keys are not used.

When imported, looks for AUTHORIZE_NET_API_URL,
AUTHORIZE_NET_HOSTED_PAYMENT_URL
AUTHORIZE_NET_KEY
and AUTHORIZE_NET_NAME
first in django.conf.settings then as os environment variables if no django.
"""
AUTHORIZE_NET_KEY = None
AUTHORIZE_NET_NAME = None
AUTHORIZE_NET_API_URL = 'https://apitest.authorize.net/xml/v1/request.api'
AUTHORIZE_NET_HOSTED_PAYMENT_URL = 'https://test.authorize.net/payment/payment'
DEFAULT_ECHECK_TYPE = "WEB"

def use_config(key, name, api_url=None, hp_url=None):
    AUTHORIZE_NET_KEY = key
    AUTHORIZE_NET_NAME = name
    if api_url is not None:
        AUTHORIZE_NET_API_URL=api_url
    if hp_url is not None:
        AUTHORIZE_NET_HOSTED_PAYMENT_URL=hp_url
    print("Config set", AUTHORIZE_NET_KEY)

def _load_config():
    _error = ValueError("Please set AUTHORIZE_NET_KEY and AUTHORIZE_NET_NAME")
    # try django first
    try:

       from django.core.exceptions import ImproperlyConfigured
       # try: No,
       from django.conf import settings
       # Don't catch t, let users know its misconfigured
       # except (ImproperlyConfigured) as e:
       #     raise AttributeError("Could not import django.conf settings,")
       AUTHORIZE_NET_KEY = settings.AUTHORIZE_NET_KEY
       # Use defaults if no django settings, for sandbox testing
       try:
           AUTHORIZE_NET_NAME = settings.AUTHORIZE_NET_NAME
       except AttributeError:
           pass
       try:
           AUTHORIZE_NET_API_URL = settings.AUTHORIZE_NET_API_URL
       except AttributeError:
           pass

    except AttributeError:
        # land here if settings doesn't contain name or key
        raise _standard_error
    except (ImportError) as e:
        # no django, or improper settings, try os environs
        import os
        AUTHORIZE_NET_API_URL = os.environ.get('AUTHORIZE_NET_API_URL')
        AUTHORIZE_NET_HOSTED_PAYMENT_URL = os.environ.get('AUTHORIZE_NET_HOSTED_PAYMENT_URL')
                                          # https://accept.authorize.net/payment/payment -> production
        AUTHORIZE_NET_NAME = os.environ.get('AUTHORIZE_NET_NAME')
        AUTHORIZE_NET_KEY = os.environ.get('AUTHORIZE_NET_KEY')

    if AUTHORIZE_NET_NAME is None or AUTHORIZE_NET_KEY is None:
        print(_error)

_load_config()
