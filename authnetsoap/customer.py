"""
Customer Profiles

Create Customer Profile
Get Customer Profile
Delete Customer Profile
Create Customer Payment Profile
Get Customer Payment Profile
Validate Customer Payment Profile - TBD, Probably needed
Delete Customer Payment Profile

Use cases

Create customer profile if none exists

Get customer profile and all payment types,
  and/or add new payment,
  and/or remove payment

"""
from .common import namespace, send, make_root, process, \
    addMerchantAuthentication, make_child, checkMessage, findall, get_unqualified_tag_name
from . import config
import lxml.etree as ET
from pdb import set_trace


def processGetCustomerProfile(dict):
    return process(getCustomerProfileRequest,
                   handleGetCustomerProfileResponse,
                   dict)


def processDeleteCustomerProfile(dict):
    return process(deleteCustomerProfileRequest,
                   handleDeleteCustomerProfileResponse,
                   dict)


def processCreateCustomerProfile(dict):
    """
    getCreateCustomerProfileRequest, send, then handleCreateCustomerProfileResponse.
    Returns handleCreateCustomerProfileResponse response data.
    """
    req = createCustomerProfileRequest(**dict)
    resp = send(req)
    resp = resp.content.decode('utf-8')
    return handleCreateCustomerProfileResponse(resp)

def createCustomerProfileRequest(email=None,
    merchantCustomerId=None,
    creditCardNumber=None,
    expirationDate=None,
    cardCode=None,
    accountType="checking",
    routingNumber=None,
    accountNumber=None,
    echeckType=config.DEFAULT_ECHECK_TYPE):
    """
    https://developer.authorize.net/api/reference/index.html#customer-profiles-create-customer-profile

    Accepts either credit card info, or bank info.

    TODO: Change input args to camelCase
    """
    root = make_root("createCustomerProfileRequest")
    addMerchantAuthentication(root)
    profile = ET.SubElement(root, "profile")
    if email is None and merchantCustomerId is None:
        raise ValueError("Need a way to ID the customer on our end (email or merchantCustomerId)")
    # Add email and/or merhcantCustomerId
    if email is not None:
        make_child(profile, "email", email)
    if merchantCustomerId is not None:
        make_child(profile, "merchantCustomerId", merchantCustomerId)
    # Add either credit card, or bank account
    # If neither present, the XML is still valid for a customer profile
    # with no payment info
    if creditCardNumber is not None:
        pay_profiles = ET.SubElement(profile, "paymentProfiles")
        payment = ET.SubElement(pay_profiles, "payment")
        cc = ET.SubElement(payment, "creditCard")
        ET.SubElement(cc, "cardNumber").text = creditCardNumber
        ET.SubElement(cc, "expirationDate").text = expirationDate
        if cardCode is not None:
            ET.SubElement(cc, "cardCode").text = cardCode
        ET.SubElement(root, "validationMode").text = "testMode"
    elif accountNumber is not None:
        pay_profiles = ET.SubElement(profile, "paymentProfiles")
        payment = ET.SubElement(pay_profiles, "payment")
        b = ET.SubElement(payment, "bankAccount")
        ET.SubElement(b, "accountType").text = accountType
        ET.SubElement(b, "accountNumber").text = accountNumber
        ET.SubElement(b, "routingNumber").text = routingNumber
        ET.SubElement(b, "echeckType").text = echeckType
    return ET.tostring(root)

def handleCreateCustomerProfileResponse(xml_string):
    """
    Return success information, or error message, as a dict.

    https://developer.authorize.net/api/reference/index.html#customer-profiles-create-customer-profile

    """
    root = ET.fromstring(xml_string)
    # element 0
    code, text, success = checkMessage(root)
    data = { "success": success, "text": text, "code": code }
    if success:
        customer_profile_id = root.find("{{{0}}}customerProfileId".format(namespace))
        data["customerProfileId"] = customer_profile_id.text
        payment_profile_ids = []
        customer_payment_profile_id_list = root.find("{{{0}}}customerPaymentProfileIdList".format(namespace))
        for element in customer_payment_profile_id_list.getchildren():
            payment_profile_ids.append(element.text)
        data['customerPaymentProfileIdList'] = payment_profile_ids
        shipping_address_ids = []
        #         customer_shipping_address_id_list = root.findall("{0}customerShippingAddressId".format(namespace))
        #         for element in customer_shipping_address_id_list:
        #             num = element.getchildren()[0].text # numericString
        #             shipping_address_ids.append(num)
        #         data['shipping_address_ids'] = shipping_address_ids
        validation_direct_responses = []
        vdrl = root.find("{{{0}}}validationDirectResponseList".format(namespace))
        for element in vdrl.getchildren():
            validation_direct_responses.append(element.text) # string
        data['validationDirectResponseList'] = validation_direct_responses
    return data


def getCustomerProfileRequest(customerProfileId=None,
    merchantCustomerId=None,
    email=None,
    refId=None,
    includeIssuerInfo=False,
    unmaskExpirationDate=False):
    """
    Builds XML request for getCustomerProfileRequest.
    https://developer.authorize.net/api/reference/index.html#customer-profiles-get-customer-profile

    Use any of customerProfileId, merchantCustomerId, or email to fetch a customer
    profile. Note that if duplicate values are stored for merchantCustomerId or
    email, you cannot use them to fetch, and must use customerProfileId.
    """
    root = make_root("getCustomerProfileRequest")
    addMerchantAuthentication(root)
    if refId:
        make_child(root, "refId", refId)
    if customerProfileId:
        make_child(root, "customerProfileId", customerProfileId)
    if merchantCustomerId:
        make_child(root, "merchantCustomerId", merchantCustomerId)
    if email:
        make_child(root, "email", email)
    if unmaskExpirationDate:
        make_child(root, "unmaskExpirationDate", "true")
    make_child(root, "includeIssuerInfo", str(includeIssuerInfo).lower())
    return ET.tostring(root, encoding='utf8', method='xml')


def handleGetCustomerProfileResponse(xml_string):
    """
    Parses XML request for getCustomerProfileResponse.
    https://developer.authorize.net/api/reference/index.html#customer-profiles-get-customer-profile

    Multiple payment types are represented by multiple paymentProfiles entities
    (children of the profile element)
    """
    root = ET.fromstring(xml_string)
    code, text, success = checkMessage(root)
    data = { "success": success, "code": code, "text": text }
    if not success:
        return data
    profile = findall(root, "profile")[0]
    ids = ['customerProfileId',
           'email',
           'description',
           'merchantCustomerId'
           ]
    for item in ids:
        elem = profile.find('{{{0}}}{1}'.format(namespace, item))
        if elem is not None:
            data[item] = elem.text
    # customerProfileId = findall(profile, "customerProfileId")[0].text
    # merchantCustomerId = findall(profile, "merchantCustomerId")[0].text
    # todo try catch for missing profile tag etc report missing tag instead of just raise error
    payment_profile_list = findall(profile, "paymentProfiles")
    data['paymentProfiles'] = []
    for payment_profile in payment_profile_list:
        # will there be more than one customer payment profile id? Yes
        customerType = findall(payment_profile, "customerType")
        if len(customerType) > 0:
            customerType = customerType[0].text
        else:
            customerType = None
        customerPaymentProfileId = findall(payment_profile, "customerPaymentProfileId")[0].text
        payment_profile_result = {"customerType": customerType,
          "customerPaymentProfileId": customerPaymentProfileId }
        payment = findall(payment_profile, "payment")
        if len(payment) < 1:
          return data
        payment = payment[0]
        # well either credit card or bank account tag is present and we can just add the child fields
        paychild = payment.getchildren()[0]
        for p in paychild.getchildren():
            payment_profile_result[get_unqualified_tag_name(p.tag)] = p.text
        data['paymentProfiles'].append({ get_unqualified_tag_name(paychild.tag) : payment_profile_result})

    # back to root for subscription ids
    # Which is an empty list if none provided from response
    # TODO: Is this tested?
    sublist = findall(root, "subscriptionIds")
    data["subscriptionIds"] = []
    if len(sublist) > 0:
        sublist = sublist[0]
        for s in sublist.getchildren():
            data["subscriptionId"].push(s.text)
    return data


def deleteCustomerProfileRequest(customerProfileId=None,
    merchantCustomerId=None,
    email=None):
    """
    Builds XML request for deleteCustomerProfileRequest.
    https://developer.authorize.net/api/reference/index.html#customer-profiles-delete-customer-profile

    Use any of customerProfileId, merchantCustomerId, or email to fetch a customer
    profile. Note that if duplicate values are stored for merchantCustomerId or
    email, you cannot use them to fetch, and must use customerProfileId.
    """
    root = make_root("deleteCustomerProfileRequest")
    addMerchantAuthentication(root)
    if customerProfileId:
        make_child(root, "customerProfileId", customerProfileId)
    if merchantCustomerId:
        make_child(root, "merchantCustomerId", merchantCustomerId)
    if email:
        make_child(root, "email", email)
    return ET.tostring(root)

def handleDeleteCustomerProfileResponse(xml_string):
    """
    Parses XML request for deleteCustomerProfileResponse.
    https://developer.authorize.net/api/reference/index.html#customer-profiles-delete-customer-profile
    """
    root = ET.fromstring(xml_string)
    code, text, success = checkMessage(root)
    data = { "success": success, "text": text, "code": code }
    return data

"""
Update Customer Payment Profile
Use this function to update a payment profile for an existing customer profile.

Note: If some fields in this request are not submitted or are submitted with a
 blank value, the values in the original profile are removed. As a best practice
  to prevent this from happening, call getCustomerPaymentProfileRequest to
   receive all current information including masked payment information.
    Change the field or fields that you wish to update, and then
    reuse all the fields you received, with updates, in a
     call to updateCustomerPaymentProfileRequest.

** WAIT, That sounds bad. Instead, why don't we, add the new payment method, and
then delete the old? **


Update Customer Profile
We can actually defer these:


def updateCustomerProfileRequest():
  \"""
  Update the customer. Does NOT allow adding payment profiles, for this, use


  If you use email to identify customer profiles, it is important to use this
  to update.
  If you use
  For: https://developer.authorize.net/api/reference/index.html#customer-profiles-update-customer-profile
  \"""
  pass

def handleUpdateCustomerProfileResponse():
  pass

"""

def createCustomerPaymentProfileRequest():
    """
    Create Customer Payment Profile
    Use this function to create a new customer payment profile for an existing
    customer profile.
    createCustomerPaymentProfileRequest
    """
    pass


def handleCreateCustomerPaymentProfileResponse():
    """
    Create Customer Payment Profile Response

    Parses createCustomerPaymentProfileResponse.
    """
    pass

"""
Get Customer Payment Profile
Use this function to retrieve the details of a customer payment
 profile associated with an existing customer profile.

Note: If the payment profile has previously been set as the default
 payment profile, you can submit this request using customerProfileId
  as the only parameter. The default payment profile is returned if a default
  payment profile has been previously designated. If no default, returns error.
"""
def getCustomerPaymentProfileRequest(customerProfileId,
    customerPaymentProfileId=None):
    pass

def handleGetCustomerPaymentProfileResponse(xml_string):
    """

    """
    root = ET.fromstring(xml_string)
    code, text, success = checkMessage(root)
    data = { "success": success, "code": code, "text": text }
    if not success:
        return data
    # todo try catch for missing profile tag etc report missing tag instead of just raise error
    payment_profile = findall(root, "paymentProfile")[0]
    # will there be more than one customer payment profile id? No bc were either getting the default or a specific profile id
    # first get customerProfileId then do payment info
    customerProfileId = findall(payment_profile, "customerProfileId")[0].text
    customerPaymentProfileId = findall(payment_profile, "customerPaymentProfileId")[0].text
    data["customerProfileId"] =customerProfileId
    data["customerPaymentProfileId"] = customerPaymentProfileId
    payment = findall(payment_profile, "payment")
    if len(payment) < 1:
      return data
    payment = payment[0]
    # well either credit card or bank account tag is present and we can just add the child fields
    paychild = payment.getchildren()[0]
    data[paychild.tag] = {}
    for p in paychild.getchildren():
        data[p.tag] = p.text
    # back to root for subscription ids
    sublist = findall(root, "subscriptionIds")
    if len(sublist) is not None:
      data["subscriptionIds"] = []
      for s in sublist.getchildren():
        data["subscriptionId"].push(s.text)
    return data
