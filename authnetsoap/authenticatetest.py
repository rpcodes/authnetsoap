"""
Test the API keys / settings.

TODO: Explain this request/reponse

processAuthenticateTestRequest is a convenience that will parse the response.
"""
from .common import addMerchantAuthentication, send, make_root, NSMAP, checkMessage
import lxml.etree as ET

from pdb  import set_trace

def processAuthenticateTestRequest():
    """
    Sends request to check if API Credentials are valid,
    and then processes the response.

    Returns true if no error code, false if creds wrong
    """
    req = getAuthenticateTestRequest()
    response = send(req)
    xml_string = response.content.decode('utf-8')
    (code, text, success ) = handleAuthenticateTestResponse(xml_string)
    return success

def handleAuthenticateTestResponse(xml_string):
    root = ET.fromstring(xml_string)
    return checkMessage(root) # ( text, code, success)

def getAuthenticateTestRequest():
    """
    Request to check if API Credentials are valid.
    """
    root = make_root("authenticateTestRequest")
    addMerchantAuthentication(root)
    return ET.tostring(root)
