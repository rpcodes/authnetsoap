"""

TODO: Complete this, adding a process method

Transaction Reporting
---------------------
Get Settled Batch List
Get Transaction List
Get Unsettled Transaction List
Get Transaction Details - Not sure if we'll want this or not even need it

"""
import datetime

from .common import make_root, addMerchantAuthentication, format_for_datetime
import lxml.etree as ET

def getUnsettledTransactionListRequest():
    """
    Provides a more immediate status of the transactions.
    Response should contain:
    <transactionStatus>capturedPendingSettlement
    """
    root = make_root("getUnsettledTransactionListRequest")
    addMerchantAuthentication(root)
    return ET.tostring(root)

def handleGetUnsettledTransactionListResponse(xml_string):
    # NOTE We have most of the code for this one in project ebsfgas
    pass

def getSettledBatchListRequest(statistics=False, firstSettlementDate=None, lastSettlementDate=None):
    """

    firstSettlementDate and lastSettlementDate may be datetime or string.

    Raises valueError if string is not in format '%Y-%m-%dT%H:%M:%SZ'
    """
    root = make_root("getSettledBatchListRequest")
    addMerchantAuthentication(root)
    stats = ET.SubElement(root, "includeStatistics")
    statistics_str = "false"
    if statistics:
        statistics_str = "true"
    stats.text = statistics_str
    # settlement date range
    if firstSettlementDate:
        text = None
        if type(firstSettlementDate) == str:
            # make sure its a real date format we wanted
            if not datetime.strptime(format_for_datetime):
                raise ValueError("String should be in format {0}".format(format_for_datetime))
            text = firstSettlementDate
        elif type(firstSettlementDate) == datetime:
            text = firstSettlementDate.strftime(format_for_datetime)
        else:
            # Dont add if we have a bad input
            # TODO: COnsider throwing a value error for this, too
            return ET.tostring(root)
        # Add it now that we know its valid
        fsd = ET.SubElement(root, "firstSettlementDate")
        fsd.text = text

        if lastSettlementDate:
            text = None
            if type(lastSettlementDate) == str:
                # make sure its a real date format we wanted
                if not datetime.strptime(format_for_datetime):
                    raise ValueError("String should be in format {0}".format(format_for_datetime))
                text = lastSettlementDate
            elif type(lastSettlementDate) == datetime:
                text = lastSettlementDate.strftime(format_for_datetime)
            else:
                # Dont add if we have a bad input
                # TODO: COnsider throwing a value error for this, too
                return ET.tostring(root)
            # Add it now that we know its valid
            lsd = ET.SubElement(root, "lastSettlementDate")
            lsd.text = text

    return ET.tostring(root)

def getTransactionList(batchID, limit=100, offset=1):
    """
    Takes batchID and option result limit and offset. Defaults to 100 and 1 respectively.
    Returns the xml as a string.
    Ordered by submit time and descending order. i.e. sends
    <sorting>
      <orderBy>submitTimeUTC</orderBy>
      <orderDescending>true</orderDescending>
    </sorting>
    """
    root = make_root("getTransactionListRequest")
    addMerchantAuthentication(root)
    ET.SubElement(root, "batchId").text = str(batchID)
    sort = ET.SubElement(root, "sorting")
    ET.SubElement(sort, "orderBy").text = "submitTimeUTC"
    ET.SubElement(sort, "orderDescending").text = "true"
    paging = ET.SubElement(root, "paging")
    ET.SubElement(paging, "limit").text = str(limit)
    ET.SubElement(paging, "offset").text = str(offset)
    return ET.tostring(root)
