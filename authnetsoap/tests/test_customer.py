"""
Connect to sandbox

Make a transaction
  then when successful

Make a profile with that payment profile

Add payment profile

Get the batches

Get the unsettled

Mock a settled and process it checking the values



"""
import unittest
from unittest.mock import patch
from datetime import datetime
import os
from authnetsoap import customer, common
from pdb import set_trace
from lxml import etree as ET
import pkg_resources

class TestCustomer(unittest.TestCase):
    """
    Unit tests against example data.

    NOTE: No API keys
    """

    get_customer_profile_response_output = {
        "code": "I00001",
        "success": True,
        "text": "Successful.",
        'customerProfileId': '1914684090',
        'description': 'Profiledescriptionhere',
        'email': 'customer-profile-email@example.com',
        'merchantCustomerId': 'Merchant_Customer_ID',
        "paymentProfiles": [
            {
                "creditCard": {
                    "cardNumber": "XXXX0012",
                    "cardType": "Discover",
                    "customerPaymentProfileId": "1828223269",
                    "customerType": None,
                    "expirationDate": "XXXX",
                    "issuerNumber": "601100"
                }
            },
            {
                "creditCard": {
                    "cardNumber": "XXXX1111",
                    "cardType": "Visa",
                    "customerPaymentProfileId": "1828223265",
                    "customerType": "individual",
                    "expirationDate": "XXXX",
                    "issuerNumber": "411111"
                }
            }
        ],
        "subscriptionIds": [],
    }

    delete_request_input = {
      "customerProfileId": "10000"
    }
    delete_response_output = {'code': 'I00001', 'success': True, 'text': 'Successful.'}


    NAME = "API_NAME"
    KEY = "API_KEY"

    def setUp(self):
        # Show long diffs
        self.maxDiff = None
        # use fake keys
        self.patcher = patch('authnetsoap.common.config',
                             AUTHORIZE_NET_KEY="API_KEY",
                             AUTHORIZE_NET_NAME="API_NAME",)
        self.patcher.start()

    def test_unit_get_customer_profile_request(self):
        get_customer_profile_request_inputs = {
                    "customerProfileId": "10000"
                }
        filepath = pkg_resources.resource_filename(__name__,
                                                   "data/getCustomerProfileRequest.xml")
        check = customer.getCustomerProfileRequest(
          **get_customer_profile_request_inputs).decode('utf-8')
        request_xml = ET.tostring(ET.parse(filepath)).decode('utf-8')
        self.assertEqual(request_xml, check,
                         "{0}\n{1}\nnot equal".format(
                             request_xml, check))

    def test_unit_handle_get_customer_profile_response(self):
        filepath = pkg_resources.resource_filename(__name__,
                                                   "data/getCustomerProfileResponseA.xml")
        get_customer_profile_response_multiple_payment_profiles = ET.tostring(ET.parse(filepath))
        check_dict = customer.handleGetCustomerProfileResponse(
            get_customer_profile_response_multiple_payment_profiles
        )
        self.assertDictEqual(TestCustomer.get_customer_profile_response_output,
                             check_dict)

    def test_delete_customer_request(self):
        filepath = pkg_resources.resource_filename(__name__,
                                                   "data/deleteCustomerProfileRequest.xml")
        check = customer.deleteCustomerProfileRequest(**TestCustomer.delete_request_input).decode('utf-8')
        request_xml = ET.tostring(ET.parse(filepath)).decode('utf-8')
        self.assertEqual(request_xml, check,
                         "{0}\n{1}\nnot equal".format(request_xml, check))

    def test_delete_customer_response(self):
        filepath = pkg_resources.resource_filename(__name__,
                                                   "data/deleteCustomerProfileResponse.xml")
        inputs = ET.tostring(ET.parse(filepath))
        check_dict = customer.handleDeleteCustomerProfileResponse(inputs)
        self.assertDictEqual(TestCustomer.delete_response_output,
                             check_dict)

    def test_unit_create_customer_profile_request(self):
        filepath = pkg_resources.resource_filename('authnetsoap',
                                                   "tests/data/createCustomerProfileRequest.xml")
        expected = ET.tostring(ET.parse(filepath)).decode('utf-8')
        # TODO: Instead of stripping all whitespace, strip all whitespace between tags.
        inputs = {
            "email": "customer-prof1ld-email@example.com",
            "creditCardNumber": "4111111111111111",
            "expirationDate": "2020-12"
        }
        check = (customer.createCustomerProfileRequest(**inputs)).decode('utf-8')
        self.assertEqual(check, expected, "{0}\n\n{1}".format(check, expected))
        pass

    def test_unit_handle_create_customer_profile_response(self):
        """
        Given a known createCustomerProfileResponse,
        ensure we recieve a dictionary with expected values.
        """
        filepath = pkg_resources.resource_filename(__name__,
                                                   "/data/createCustomerProfileResponse.xml")
        inputs = ET.tostring(ET.parse(filepath))

        expected = {
            "success": True,
            "code": "I00001",
            "text": "Successful.",
            "validationDirectResponseList": ["1,1,1,(TESTMODE) This transaction has been approved.,000000,P,0,none,Test transaction for ValidateCustomerPaymentProfile.,1.00,CC,auth_only,Merchant_Customer_ID,,,,,,,,,,,customer-prof1ld-email@example.com,,,,,,,,,0.00,0.00,0.00,FALSE,none,8CDC5494AF4CB9545C05E36A78141BB9,,,,,,,,,,,,,XXXX1111,Visa,,,,,,,,,,,,,,,,,"],
            "customerProfileId": "1914570622",
            "customerPaymentProfileIdList": ["1828132866"],
        }
        check = customer.handleCreateCustomerProfileResponse(inputs)
        for cc in check:
            self.assertEqual(expected[cc], check[cc], "\n{0}\n{1}".format(expected[cc], check[cc]))

    def tearDown(self):
        self.patcher.stop()


if __name__ == "__main__":
    unittest.main()
