"""
Connect to sandbox

Make a transaction
  then when successful

Make a profile with that payment profile

Add payment profile

Get the batches

Get the unsettled

Mock a settled and process it checking the values
"""
import unittest
from unittest.mock import patch
from datetime import datetime
import os
import authnetsoap
from pdb import set_trace

class Tests(unittest.TestCase):
    """
    TODO: Some of the tests, like tests_online_xyz, are most suited to being
    in a functional test with configurable authorization parameters!
    """
    # sandbox credentials, don't run tests on production
    API_URL = 'https://apitest.authorize.net/xml/v1/request.api'
    KEY = '8qc2A2q65h5YJB94'
    NAME = '3C47tKn3T'

    def setUp(self):
        """
        Overrides config values with sandbox values, otherwise we risk
        someone testing against production values.
        We use common.py to create authentication info so we mock that
        version of config. __main__ etc doesnt point to the right isinstance
        """
        self.patcher = patch('authnetsoap.request.config',
          AUTHORIZE_NET_KEY=Tests.KEY,
          AUTHORIZE_NET_NAME=Tests.NAME,
          AUTHORIZE_NET_API_URL=Tests.API_URL)
        self.patcher.start()

    # def test_online_auth(self):
    #     """
    #     Uses sandbox credentials to connect to live server.
    #
    #     Sends authenticateTestRequest, then processes the response.
    #     """
    #     self.assertTrue(authenticatetest.processAuthenticateTestRequest())
    #
    # def test_online_process_create_customer_profile(self):
    #     """
    #     Uses sandbox credentials to connect to live server.
    #
    #     Sends createCustomerProfileRequest with payment info (cc). Processes
    #     the response.
    #     """
    #     uid = "{0}@here.com".format(datetime.now())
    #     inputs = {
    #         "email": uid,
    #         "credit_card_number": "4111111111111111",
    #         "expiration_date": "2020-12"
    #     }
    #     expected = {
    #         "success": True,
    #     }
    #     check = customer.processCustomerProfile(inputs)
    #     for e in expected:
    #         self.assertEqual(check[e], expected[e],
    #                           "{0} {1}".format(check[e], expected[e]))

    def tearDown(self):
        self.patcher.stop()

if __name__ == "__main__":
    unittest.main()
