
def dictHpReturnOptions(showReceipt=True, url="", urlText="", cancelUrl="", cancelUrlText=""):
    return {"showReceipt": showReceipt,
     "url": url,
     "urlText": urlText,
     "cancelUrl": cancelUrl,
     "cancelUrlText": cancelUrlText}

def dictHpPaymentOptions(cardCodeRequired=False, showCreditCard=True, showBankAccount=True):
    return { "cardCodeRequired": cardCodeRequired,
             "showCreditCard": showCreditCard,
             "showBankAccount": showBankAccount}

def addHostedPaymentPageSettings(hprequest_xml,
                                iframe_url=None,
                                 returnOptions=dictHpReturnOptions(),

    ):
    root = hprequest_xml

    """
    <hostedPaymentSettings>
    <setting>
      <settingName>hostedPaymentReturnOptions</settingName>
      <settingValue>{"showReceipt": true, "url": "https://mysite.com/receipt", "urlText": "Continue", "cancelUrl": "https://mysite.com/cancel", "cancelUrlText": "Cancel"}</settingValue>
    </setting>
    <setting>
      <settingName>hostedPaymentButtonOptions</settingName>
      <settingValue>{"text": "Pay"}</settingValue>
    </setting>
    <setting>
      <settingName>hostedPaymentStyleOptions</settingName>
      <settingValue>{"bgColor": "blue"}</settingValue>
    </setting>
    <setting>
      <settingName>hostedPaymentPaymentOptions</settingName>
      <settingValue>{"cardCodeRequired": false, "showCreditCard": true, "showBankAccount": true}</settingValue>
    </setting>
    <setting>
      <settingName>hostedPaymentSecurityOptions</settingName>
      <settingValue>{"captcha": false}</settingValue>
    </setting>
    <setting>
      <settingName>hostedPaymentShippingAddressOptions</settingName>
      <settingValue>{"show": false, "required": false}</settingValue>
    </setting>
    <setting>
      <settingName>hostedPaymentBillingAddressOptions</settingName>
      <settingValue>{"show": true, "required":false}</settingValue>
    </setting>
    <setting>
      <settingName>hostedPaymentCustomerOptions</settingName>
      <settingValue>{"showEmail": false, "requiredEmail": false, "addPaymentProfile": true}</settingValue>
    </setting>
    <setting>
      <settingName>hostedPaymentOrderOptions</settingName>
      <settingValue>{"show": true, "merchantName": "G and S Questions Inc."}</settingValue>
    </setting>
    <setting>
      <settingName>hostedPaymentIFrameCommunicatorUrl</settingName>
      <settingValue>{"url": "https://mysite.com/special"}</settingValue>
    </setting>
  </hostedPaymentSettings>
  """



def getHostedPaymentPageRequest():
    pass



def addTransactionWithEmail(root, amount, email=None, invoice_data=None):
    """
    invoice_data.invoiceNumber shoud be unique across all invoice types,
    as applicable. It will be used across any invoice type, to lookup and
    update records, so additional
    tables may be needed to associate this ID to the real database table and id.
    Since this is for internal usage, we don't need to log this the same way as
    a transaction ID (i.e. confirmation number)

    Always sets
    transactionType>authCaptureTransaction

    """
    tr = ET.SubElement(root, "transactionRequest")
    ET.SubElement(tr, "transactionType").text = "authCaptureTransaction"
    ET.SubElement(tr, "amount").text = "amount"
    if invoice_data is not None:
        addOrder(tr, invoice_data)
    if email is not None:
        addCustomerOnlyEmail(tr, email)
    return tr

def addCustomerOnlyEmail(tr, email):
    customer  = ET.SubElement(tr, "customer")
    ET.SubElement(customer, "email").text = email
    return customer

def addOrder(transactionRequest, invoice_data):
    """
    Expects dict:
    {
      "invoiceNumber": 123,
      "description": None # or string
    }
    """
    order = ET.SubElement(transactionRequest, "order")
    ET.SubElement(order, "invoiceNumber").text = invoice_data.invoiceNumber
    if invoice_data.description is not None:
        ET.SubElement(order, "description").text = invoice_data.description
    return order
